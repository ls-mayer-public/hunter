# Copyright (c) 2017, Tobias Kölling
# All rights reserved.

# !!! DO NOT PLACE HEADER GUARDS HERE !!!

include(hunter_add_version)
include(hunter_cacheable)
include(hunter_download)
include(hunter_pick_scheme)

hunter_add_version(
    PACKAGE_NAME
    Date
    VERSION
    "2.3"
    URL
    "https://gitlab.lrz.de/ls-mayer-public/date/repository/e92510cc3a548d2c1f99e985e4bf39d3807ba871/archive.tar.gz"
    SHA1
    545168a46b5a76aab79451c9008d45757e0e78ac
)

hunter_pick_scheme(DEFAULT url_sha1_cmake)
hunter_cacheable(Date)
hunter_download(PACKAGE_NAME Date) 
